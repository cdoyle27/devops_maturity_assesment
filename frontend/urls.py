"""devops_maturity_assessment_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from .views import AssessmentTemplateView, CategoryCreate, AssessmentCreate, AssessmentView
urlpatterns = [
    path('<int:pk>', AssessmentTemplateView.as_view(), name='assessment__template_view'),
    path('category_create', CategoryCreate.as_view(), name='category_create'),
    path('assessment_create', AssessmentCreate.as_view(), name='assessment_create'),
    path('assessment_view/<int:pk>', AssessmentView.as_view(), name='assessment_view')
]
