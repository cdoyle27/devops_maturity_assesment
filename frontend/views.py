from django.views.generic import DetailView, CreateView
from assessments.models import AssessmentTemplate, Assessment
from assessments.forms import CategoryForm, AssessmentForm


class AssessmentTemplateView(DetailView):
    model = AssessmentTemplate
    template_name = "assessment_template_view.html"
    context_object_name = "assessment_template"


class CategoryCreate(CreateView):
    form_class = CategoryForm
    template_name = "category_create.html"


class AssessmentCreate(CreateView):
    form_class = AssessmentForm
    template_name = "assessment_create.html"


class AssessmentView(DetailView):
    model = Assessment
    template_name = "assessment_view.html"
    context_object_name = "assessment"
