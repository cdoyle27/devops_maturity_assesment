from django.contrib import admin

# Register your models here.
from .models import AssessmentTemplate, Category, Area, Answer, Question, Grade, Assessment

admin.site.register(Category)
admin.site.register(AssessmentTemplate)
admin.site.register(Area)
admin.site.register(Answer)
admin.site.register(Question)
admin.site.register(Grade)
admin.site.register(Assessment)
