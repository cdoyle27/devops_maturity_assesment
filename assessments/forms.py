from django import forms
from .models import Category, Assessment


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name']


class AssessmentForm(forms.ModelForm):
    class Meta:
        model = Assessment
        fields = ['assessment_template', 'answers']
